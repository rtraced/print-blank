import React from "react"
import GlobalStyle from "./components/GlobalStyle"
import BlankContent from "./components/BlankContent"

// Неизвестно, доступен ли объект URL в оболочке
const url = new URL(window.location.href)
const data = url.searchParams.get("data")
const reservation = JSON.parse(data)

const Blank = () => (
  <React.Fragment>
    <GlobalStyle />
    <BlankContent data={reservation} />
  </React.Fragment>
)

export default Blank
