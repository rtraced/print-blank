import moment from "moment"
import numberToWords from "./numberToWords"

const getProcessedServices = services => {
  const result = services.filter(s => s.service !== "Улучшение").map(s => ({
    name: s.service,
    quantity: s.quantity,
    price: s.price,
    totalPrice: Math.round(s.price * s.quantity)
  }))

  const groupedUpgrades = {}

  services.filter(s => s.service === "Улучшение").forEach(s => {
    const price = Math.round(s.price)

    if (!(price in groupedUpgrades)) groupedUpgrades[price] = 0

    groupedUpgrades[price] += 1
  })

  for (let price in groupedUpgrades) {
    result.push({
      name: "Улучшение",
      quantity: groupedUpgrades[price],
      price,
      totalPrice: price * groupedUpgrades[price]
    })
  }

  return result
}

export default data => {
  let guestName = ""
  let roomID = ""
  let formattedCheckIn = ""
  let formattedCheckOut = ""
  let daysTotalPrice = ""
  let processedServices = []
  let servicesTotalPrice = ""
  let totalPrice = ""
  let totalPriceString = ""
  let paymentDate = ""

  if (data) {
    const { reserved_days: days, additional_services: services } = data

    guestName = data.guest_name
    roomID = days.length && days[0].room && days[0].room.room_id

    formattedCheckIn = moment(
      `${data.start} ${data.check_in_time}`,
      "YYYY-MM-DD HH:mm:ss"
    ).format("DD.MM.YYYY; HH:mm")
    formattedCheckOut = moment(
      `${data.end} ${data.check_out_time}`,
      "YYYY-MM-DD HH:mm:ss"
    ).format("DD.MM.YYYY; HH:mm")

    daysTotalPrice = Math.round(
      days.reduce((acc, d) => {
        console.log(acc)
        const r = acc + (d.payment_date ? d.price : 0)
        return r
      }, 0)
    )
    processedServices = getProcessedServices(services)
    servicesTotalPrice = processedServices.reduce(
      (acc, s) => acc + s.totalPrice,
      0
    )
    totalPrice = daysTotalPrice + servicesTotalPrice
    totalPriceString = numberToWords(totalPrice)

    paymentDate = moment().format("DD.MM.YYYY")
  }

  return {
    guestName,
    roomID,
    formattedCheckIn,
    formattedCheckOut,
    daysTotalPrice,
    processedServices,
    servicesTotalPrice,
    totalPrice,
    totalPriceString,
    paymentDate
  }
}
