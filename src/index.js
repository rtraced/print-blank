import React from "react"
import ReactDOM from "react-dom"
import Blank from "./Blank"

ReactDOM.render(<Blank />, document.getElementById("root"))
