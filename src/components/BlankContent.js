import React from "react"
import styled from "styled-components"

import { Field, Name, Text } from "./FieldUtils"
import Specification from "./Specification"
import ReceiptHeader from "./ReceiptHeader"
import SummaryTable from "./SummaryTable"
import getValuesFromData from "../helpers/getValuesFromData"

const A4 = styled.div`
  width: 21cm;
  min-height: 29.7cm;
  padding: 0.8cm 1.2cm;
`

const BlankContent = props => {
  const { data } = props

  const values = getValuesFromData(data)

  return (
    <A4>
      <Specification />
      <Field>
        <Name>Исполнитель</Name>
        <Text
          stretch
          caption="(наименование и организационно-правовая форма — для организаций"
        />
      </Field>
      <Field>
        <Text
          stretch
          caption="фамилия, имя, отчество — для индивидуального предпринимателя)"
        >
          ИП Милосердов Вадим Викторович
        </Text>
      </Field>
      <Field>
        <Text
          stretch
          caption="(место нахождения постоянно действующего исполнительного органа юр. лица, иного органа или лица, имеющего право действовать от имени юр. лица без доверенности)"
          captionLines={2}
        >
          127473, г. Москва, пер. 1-й Самотечный, 18-48
        </Text>
      </Field>
      <Field>
        <Name>ИНН</Name>
        <Text width={30}>371400992957</Text>
      </Field>

      <ReceiptHeader />

      <Field>
        <Name>Ф.И.О.</Name>
        <Text stretch>{values.guestName}</Text>
      </Field>
      <Field>
        <Name>Номер комнаты</Name>
        <Text width={10}>{values.roomID}</Text>
        <Name>Номер корпуса</Name>
        <Text width={10} />
        <Name>Заезд</Name>
        <Text stretch caption="(дата, часы)">
          {values.formattedCheckIn}
        </Text>
      </Field>

      <SummaryTable values={values} />

      <Field>
        <Name>Полная стоимость услуги</Name>
        <Text stretch notLast caption="(сумма прописью)">
          {values.totalPriceString}
        </Text>
        <Name>руб.</Name>
        <Text width={10}>00</Text>
        <Name>коп.</Name>
      </Field>
      <Field>
        <Name>Со ст. 779-783 ГК РФ ознакомлен</Name>
        <Text width={20} caption="(заказчик, подпись)" />
      </Field>
      <Field>
        <Name>Лицо ответственное за оформление заказа</Name>
        <Text stretch caption="(должность, Ф.И.О., подпись)" />
      </Field>
      <Field>
        <Name bold>Оплачено:</Name>
      </Field>
      <Field>
        <Name>
          наличными денежными средствами или использованием платежных карт
        </Name>
      </Field>
      <Field>
        <Text width={10}>{values.totalPrice}</Text>
        <Name>руб.</Name>
        <Text width={10}>00</Text>
        <Name>коп.</Name>
        <Text stretch notLast caption="(сумма прописью)">
          {values.totalPriceString}
        </Text>
        <Name>руб.</Name>
        <Text width={10}>00</Text>
        <Name>коп.</Name>
      </Field>
      <Field>
        <Name>Заказчик</Name>
        <Text width={10} />
        <Name>.</Name>
        <Text width={20} caption="(подпись, дата)" />
      </Field>
      <Field>
        <Name>
          Получено лицом ответственным за совершение операции и правильности
        </Name>
      </Field>
      <Field>
        <Name>ее оформления</Name>
        <Text
          width={45}
          caption="(дата, должность исполнителя, Ф.И.О., подпись)"
        />
        <Name>Дата осуществления расчета</Name>
        <Text stretch>{values.paymentDate}</Text>
      </Field>
      <Field>
        <Name>Услуга получена</Name>
        <Text width={20} />
        <Name>.</Name>
        <Text width={40} caption="(клиент, подпись, дата)" />
      </Field>
    </A4>
  )
}

export default BlankContent
