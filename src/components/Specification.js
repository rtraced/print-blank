import React from "react"
import styled from "styled-components"

const StyledSpecification = styled.div`
  font-size: 7pt;
  margin-bottom: 4mm;
`

const Specification = () => (
  <StyledSpecification>
    Утверждена приказом Объединения работодателей «Росбытсоюз» от 30 июня 2008
    г. № 14 в соответствии с Постановлением Правительства РФ от 06.05.7008 г. №
    359, Заказ ООО «Типографсервис», ИНН 1234512345, г. Москва, ул. Тверская, д.
    3, заказ № 3 2016 года, тираж 300 экз.
  </StyledSpecification>
)

export default Specification
