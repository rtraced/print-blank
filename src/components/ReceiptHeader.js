import React from "react"
import styled from "styled-components"

const ReceiptHeaderTitle = styled.p`
  margin: 0;
  font-size: 12pt;
  line-height: 1.4;
  font-weight: bold;
`

const ReceiptHeaderDescription = styled.p`
  font-size: 10pt;
  line-height: 1.6;
  margin: 3mm 0 0 0;
`

const ReceiptHeaderInfo = () => (
  <div>
    <ReceiptHeaderTitle>
      КВИТАНЦИЯ-ДОГОВОР
      <br />№ 00011
    </ReceiptHeaderTitle>
    <ReceiptHeaderDescription>
      на услуги мотелей, кемпингов, гостиниц
      <br />
      Серия АИ
    </ReceiptHeaderDescription>
  </div>
)

const ReceiptHeaderTableCell = styled.td`
  padding: 2mm 4mm;
  border: 1px solid black;
  font-size: ${props => props.fontSize};
  text-align: center;
`

const StyledReceiptHeaderTable = styled.table`
  width: 6cm;
  border-collapse: collapse;
  font-size: 10pt;
`

const ReceiptHeaderTable = () => (
  <StyledReceiptHeaderTable>
    <thead>
      <tr>
        <ReceiptHeaderTableCell colSpan={2}>
          Форма <b>БО-18</b>
        </ReceiptHeaderTableCell>
      </tr>
    </thead>
    <tbody>
      <tr>
        <ReceiptHeaderTableCell>Код услуги</ReceiptHeaderTableCell>
        <ReceiptHeaderTableCell fontSize="12pt">041021</ReceiptHeaderTableCell>
      </tr>
    </tbody>
  </StyledReceiptHeaderTable>
)

const StyledReceiptHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 6mm 0;
`

const ReceiptHeader = () => (
  <StyledReceiptHeader>
    <ReceiptHeaderInfo />
    <ReceiptHeaderTable />
  </StyledReceiptHeader>
)

export default ReceiptHeader
